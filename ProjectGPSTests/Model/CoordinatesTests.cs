﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGPS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model.Tests
{
    [TestClass()]
    public class CoordinatesTests
    {
        [TestMethod()]
        public void CoordinatesLatitudeTest()
        {
            Coordinates coordinates = new Coordinates(1, 1);
            Assert.IsTrue(coordinates.Latitude < 90.0);
            Assert.IsTrue(coordinates.Latitude > -90.0);
            Assert.IsTrue(coordinates.Latitude == 1.0);
        }

        [TestMethod()]
        public void CoordinatesLongitudeTest()
        {
            Coordinates coordinates = new Coordinates(1, 1);
            Assert.IsTrue(coordinates.Longitude < 180.0);
            Assert.IsTrue(coordinates.Longitude > -180.0);
            Assert.IsTrue(coordinates.Longitude == 1.0);
        }
        [TestMethod()]
        public void CoordinatesLatitudeXYZTest()
        {
            Coordinates coordinates = new Coordinates(1, 1, 0);
            Assert.IsTrue(coordinates.Latitude < 90.0);
            Assert.IsTrue(coordinates.Latitude > -90.0);
        }

        [TestMethod()]
        public void CoordinatesLongitudeXYZTest()
        {
            Coordinates coordinates = new Coordinates(1, 1, 0);
            Assert.IsTrue(coordinates.Longitude < 180.0);
            Assert.IsTrue(coordinates.Longitude > -180.0);
        }
    }
}