﻿using Microsoft.Maps.MapControl.WPF;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGPS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model.Tests
{
    [TestClass()]
    public class ShareInfoTests
    {
        [TestMethod()]
        public void ShareInfoTest()
        {
            ObservableCollection<Picture> pic = new ObservableCollection<Picture>();
            pic.Add(new Picture(@"C:\Users\Piotr\Documents\latarnie\photos\DSCN0850.jpg"));
            ShareInfo s = new ShareInfo();
            s.Pictures = pic;
            Assert.AreEqual(1, s.Pictures.Count);
        }
        [TestMethod()]
        public void ShareInfoLocationTest()
        {
            Location loc = new Location(1, 2);
            ObservableCollection<Picture> pic = new ObservableCollection<Picture>();
            pic.Add(new Picture(@"C:\Users\Piotr\Documents\latarnie\photos\DSCN0850.jpg"));
            ShareInfo s = new ShareInfo();
            s.Location = loc;
            s.Pictures = pic;
            Assert.AreEqual(1, s.Location.Latitude);
            Assert.AreEqual(2, s.Location.Longitude);
        }
    }
}