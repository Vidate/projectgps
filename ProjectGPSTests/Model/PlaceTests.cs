﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Maps.MapControl.WPF;
using ProjectGPS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.ViewModel.Tests
{
    [TestClass()]
    public class PlaceTests
    {
        //TODO zmienic testy
        [TestMethod()]
        public void PlacesLocationTest()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Place places = new Place();
            places.SetCoordinatesFromImagePath = path;
            Assert.IsTrue(places.GetLocation != null);
        }
        //[TestMethod()]
        //public void PlacesColorTest()
        //{
        //    string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
        //    Place places = new Place();
        //    places.SetCoordinatesFromImagePath = path;

        //    places.Color = "Red";
        //    string expected = "Red";
        //    Assert.AreEqual(expected, places.Color);
        //    Assert.IsFalse(string.IsNullOrEmpty(places.Color));
        //}

        [TestMethod()]
        public void PlacesLatitudeUnder90Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Place places = new Place();
            places.SetCoordinatesFromImagePath = path;
            Assert.IsTrue(places.GetLatitudeValue < 90.0);
        }
        [TestMethod()]
        public void PlacesLongitudeUnder90Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Place places = new Place();
            places.SetCoordinatesFromImagePath = path;
            Assert.IsTrue(places.GetLongitudeValue < 180.0);
        }
        [TestMethod()]
        public void PlacesLatitudeHigherM90Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Place places = new Place();
            places.SetCoordinatesFromImagePath = path;
            Assert.IsTrue(places.GetLatitudeValue > -90.0);
        }
        [TestMethod()]
        public void PlacesLongitudeHigherM90Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Place places = new Place();
            places.SetCoordinatesFromImagePath = path;
            Assert.IsTrue(places.GetLongitudeValue > -180.0);
        }
    }
}