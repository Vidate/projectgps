﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectGPS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model.Tests
{
    [TestClass()]
    public class PictureTests
    {
        //TODO dokonczyc
        [TestMethod()]
        public void PicturePathTest()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Picture picture = new Picture(path);
            Assert.IsFalse(string.IsNullOrEmpty(picture.GetPicturePath));
        }
        [TestMethod()]
        public void PicturePathArgTest()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Picture picture = new Picture(path);
            Assert.AreEqual(path, picture.GetPicturePath);

        }
        [TestMethod()]
        public void PictureLatitudeUnder90Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Picture picture = new Picture(path);
            Assert.IsTrue(picture.GetLatitudeValue < 90.0);
        }
        [TestMethod()]
        public void PictureLongitudeUnder180Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Picture picture = new Picture(path);
            Assert.IsTrue(picture.GetLongitudeValue < 180.0);
        }
        [TestMethod()]
        public void PictureLatitudeHigherM90Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Picture picture = new Picture(path);
            Assert.IsTrue(picture.GetLatitudeValue > -90.0);
        }
        [TestMethod()]
        public void PictureLongitudeHigherM180Test()
        {
            string path = @"C:\Users\Piotr\Documents\latarnie\photos\DSCN0849.jpg";
            Picture picture = new Picture(path);
            Assert.IsTrue(picture.GetLongitudeValue > -180.0);
        }
    }
}