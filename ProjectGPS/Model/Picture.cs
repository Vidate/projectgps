﻿using ExifLib;
using Microsoft.Maps.MapControl.WPF;
using ProjectGPS.Model.Interfaces;
using ProjectGPS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model
{
    //TODO Unit test
    

    public class Picture : IPicture
    {
        private string _picturePath;
        private Location _location;
        private IUnitCoordinate _latitude;
        private IUnitCoordinate _longitude;
        private double _distance;

        public Picture(string imgPath)
        {
            if (imgPath != null)
            {
                _picturePath = imgPath;
                double[] tmpLong = new double[] { 0, 0, 0 };
                double[] tmpLat = new double[] { 0, 0, 0 };
                using (ExifReader reader = new ExifReader(imgPath))
                {
                    reader.GetTagValue<double[]>(ExifTags.GPSLongitude, out tmpLong);
                    reader.GetTagValue<double[]>(ExifTags.GPSLatitude, out tmpLat);
                }
                _longitude = new Longitude(tmpLong);
                _latitude = new Latitude(tmpLat);
                _location = new Location(GetLatitudeValue, GetLongitudeValue);
            }
        }
        public string SetPicturePath
        {
            set {  _picturePath = value; }
        }

        public string GetPicturePath
        {
            get { return _picturePath; }
        }
        public Location GetLocation
        {
            get { return _location; }
        }
        public double GetLatitudeValue
        {
            get { return _latitude.GetValue; }
        }
        public double GetLongitudeValue
        {
            get { return _longitude.GetValue; }
        }
        public double Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }
    }


}
