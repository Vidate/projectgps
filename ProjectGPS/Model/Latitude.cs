﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.ViewModel
{
    public class Latitude : IUnitCoordinate
    {
        public Latitude(double[] gpsLatitude)
        {
            if (gpsLatitude.Length == 3)
            {
                _gpsDegrees = gpsLatitude[0];
                _gpsMinutes = gpsLatitude[1];
                _gpsSeconds = gpsLatitude[2];
                _latitudeValue = _gpsDegrees + (_gpsMinutes / 60) + (_gpsSeconds / 3600);
            }
        }
        public Latitude(double Latitude)
        {
            _latitudeValue = Latitude;
        }
        private double _gpsDegrees;
        private double _gpsMinutes;
        private double _gpsSeconds;
        private double _latitudeValue;
        public double GetValue { get { return _latitudeValue; }}
        public double GetDegrees { get { return _gpsDegrees; } set { _gpsDegrees = value; } }
        public double GetMinutes { get { return _gpsMinutes; } set { _gpsMinutes = value; } }
        public double GetSeconds { get { return _gpsSeconds; } set { _gpsSeconds = value; } }
        public override string ToString()
        {
            return "Szerokość:"
                + "\r\nStopnie: " + _gpsDegrees
                + "\r\nMinuty: " + _gpsMinutes
                + "\r\nSekundy: " + _gpsSeconds;
        }
    }
}

