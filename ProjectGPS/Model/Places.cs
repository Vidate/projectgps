using ExifLib;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetTopologySuite.IO;
using ProjectGPS.Model;
using ProjectGPS.Model.Interfaces;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace ProjectGPS.ViewModel
{
    //TODO Unit test
    //Interface
   
    public class Place : IPlace
    {
        private string _description;    
        private Location _location;
        private IUnitCoordinate _latitude;
        private IUnitCoordinate _longitude;
        private SolidColorBrush _color;
        private ObservableCollection<Picture> _pictures;
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public SolidColorBrush Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public Place()
        {
            Pictures = new ObservableCollection<Picture>();
            Color = Brushes.AliceBlue;
        }

        public Coordinates SetCoordinates
        {
            set
            {
                _latitude = new Latitude(value.Latitude);
                _longitude = new Longitude(value.Longitude);
                _location = new Location(value.Latitude, value.Longitude);
            }
        }
        public string SetCoordinatesFromImagePath
        {
            set
            {
                double[] tmpLong;
                double[] tmpLat;
                using (ExifReader reader = new ExifReader(value))
                {
                    reader.GetTagValue<double[]>(ExifTags.GPSLongitude, out tmpLong);
                    reader.GetTagValue<double[]>(ExifTags.GPSLatitude, out tmpLat);
                }
                _longitude = new Longitude(tmpLong);
                _latitude = new Latitude(tmpLat);
                _location = new Location(GetLatitudeValue, GetLongitudeValue);
                _description = "Szerokosc: "
                    + Math.Round(GetLatitudeValue, 5).ToString()
                    + "\r\nDługość: "
                    + Math.Round(GetLongitudeValue, 5).ToString();
            }
        }

        public Location GetLocation
        {
            get { return _location; }
        }
        public double GetLatitudeValue
        {
            get { return _latitude.GetValue; }
        }
        public double GetLongitudeValue
        {
            get { return _longitude.GetValue; }
        }
        public string GetDescription
        {
            get { return _description; }
        }

        public ObservableCollection<Picture> Pictures
        {
            get
            {
                return _pictures;
            }

            set
            {
                _pictures = value;
            }
        }
    }
}
