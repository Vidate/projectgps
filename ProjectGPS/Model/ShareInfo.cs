﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model
{
    public class ShareInfo : IShareInfo
    {
        private ObservableCollection<Picture> _pictures;
        private Location _location;

        public ObservableCollection<Picture> Pictures
        {
            get { return _pictures; }
            set { _pictures = value; }
        }

        public Location Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public ShareInfo()
        {
        }
    }
}
