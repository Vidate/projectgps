﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.ViewModel
{
    public class Longitude : IUnitCoordinate
    {
        public Longitude(double[] gpsLongitude)
        {
            if (gpsLongitude.Length == 3)
            {
                _gpsDegrees = gpsLongitude[0];
                _gpsMinutes = gpsLongitude[1];
                _gpsSeconds = gpsLongitude[2];
                _longitudeValue = _gpsDegrees + (_gpsMinutes / 60) + (_gpsSeconds / 3600);
                //_longitudeValue =-(_gpsDegrees + (_gpsMinutes / 60) + (_gpsSeconds / 3600));
            }
        }
        public Longitude(double gpsLongitude)
        {
            _longitudeValue =  gpsLongitude;
        }
        private double _gpsDegrees;
        private double _gpsMinutes;
        private double _gpsSeconds;
        private double _longitudeValue;
        public double GetValue { get { return _longitudeValue; } }
        public double GetDegrees { get { return _gpsDegrees; } set { _gpsDegrees = value; } }
        public double GetMinutes { get { return _gpsMinutes; } set { _gpsMinutes = value; } }
        public double GetSeconds { get { return _gpsSeconds; } set { _gpsSeconds = value; } }
        public override string ToString()
        {
            return "Długość:"
                + "\r\nStopnie: " + _gpsDegrees
                + "\r\nMinuty: " + _gpsMinutes
                + "\r\nSekundy: " + _gpsSeconds;
        }
    }
}
