﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model.Interfaces
{
    public interface IPicture
    {
        string SetPicturePath { set; }
        string GetPicturePath { get; }
        Location GetLocation { get; }
        double GetLatitudeValue { get; }
        double GetLongitudeValue { get; }
        double Distance { get; set; }
    }
}
