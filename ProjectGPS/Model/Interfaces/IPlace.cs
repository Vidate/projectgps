﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ProjectGPS.Model.Interfaces
{
    public interface IPlace
    {
        SolidColorBrush Color { get; set; }
        Location GetLocation { get; }
        double GetLatitudeValue { get; }
        double GetLongitudeValue { get; }
        string GetDescription { get; }
        Coordinates SetCoordinates { set; }
        string SetCoordinatesFromImagePath { set; }
        ObservableCollection<Picture> Pictures { get; set; }
        int Id { get; set; }
    }
}
