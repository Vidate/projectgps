﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model.Interfaces
{
    public interface IPlaceInfo
    {
        Location Location { get; set; }
        ObservableCollection<Picture> Pictures { get; set; }
    }
}
