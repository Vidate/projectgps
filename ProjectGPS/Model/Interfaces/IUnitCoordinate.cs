﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.ViewModel
{
    interface IUnitCoordinate
    {
        double GetDegrees { get; set; }
        double GetMinutes { get; set; }
        double GetSeconds { get; set; }
        double GetValue { get; }

    }
}
