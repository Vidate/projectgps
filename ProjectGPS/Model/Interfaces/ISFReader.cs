﻿using ProjectGPS.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model.Interfaces
{
    public interface ISFReader
    {
        ObservableCollection<Coordinates> Coordinates { get; set; }
        ObservableCollection<Place> Places { get; set; }
        string FileName { get; set; }
        string PathToFolder { get; set; }
        void ReadShapeFile();
        void Save(ObservableCollection<Place> places);
    }
}
