﻿using System.Collections.ObjectModel;
using Microsoft.Maps.MapControl.WPF;

namespace ProjectGPS.Model
{
    public interface IShareInfo
    {
        Location Location { get; set; }
        ObservableCollection<Picture> Pictures { get; set; }
    }
}