﻿using DotSpatial.Projections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model
{

    public class Coordinates : ICoordinates
    {
        private double _longitude;
        private double _latitude;

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        //    LAT = latitude* pi/180
        //LON = longitude* pi/180
        //x = -R* cos(LAT) * cos(LON)
        //y =  R* sin(LAT)
        //z =  R* cos(LAT) * sin(LON)
        public Coordinates(double longitude, double latitude)
        {
            _longitude = longitude;
            _latitude = latitude;
        }
        public Coordinates(double x, double y, double z)
        {
            FromXYZToCoordinates(x, y, z);
        }
        //TODO zrobic poprawna konversje 
        private void FromXYZToCoordinates(double x, double y, double z)
        {
            double R = 6378137; //metry
            double Lat = Math.Sin(y / R);
            double Long = Math.Cos(x / (-R * Math.Cos(Lat)));
            _latitude = Lat / (Math.PI / 180);
            _longitude = Long / (Math.PI / 180);
            //Defines the starting coordiante system
            //ProjectionInfo pStart = KnownCoordinateSystems.Geographic.World.GRS80;
            ////Defines the ending coordiante system
            //ProjectionInfo pEnd = KnownCoordinateSystems.Projected.NorthAmerica.USAContiguousLambertConformalConic;
            ////Calls the reproject function that will transform the input location to the output locaiton
            //Reproject.ReprojectPoints( new double[] { x, y },
            //                           new double[] { z },
            //                           pStart,
            //                           pEnd,
            //                           0,
            //                           1);
        }
        public double XValue 
        {
            get
            {
                var lat = _latitude*Math.PI/180;
                var lon = _longitude*Math.PI/180;
                return -6371000*Math.Cos(lat) * Math.Cos(lon);
            }
        }
        public double YValue 
        {
            get
            {
                var lat = _latitude * Math.PI / 180;
                return 6371000*Math.Sin(lat);
            }
        }
    }
}