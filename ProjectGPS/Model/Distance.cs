﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGPS.Model
{
    //TODO Unit test

    public class Distance
    {
        private double _meters;
        public Distance(Location location1, Location location2)
        {
            double R = 6371000; // meters
            double dLat = ToRadians((location2.Latitude - location1.Latitude));
            double dLon = ToRadians((location2.Longitude - location1.Longitude));
            double lat1 = ToRadians(location1.Latitude);
            double lat2 = ToRadians(location2.Latitude);
            double a =
                Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2) *
                Math.Cos(lat1) * Math.Cos(lat2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            _meters = R * c;
        }

        public double ToRadians(double value)
        {
            return (Math.PI / 180) * value;
        }

        public double Value
        {
            get { return _meters; }
            set { _meters = value; }
        }
    }
}
