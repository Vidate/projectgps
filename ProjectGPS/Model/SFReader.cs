using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetTopologySuite.IO;
using System.Collections.ObjectModel;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using ProjectGPS.Model.Interfaces;
using System.IO;
using ProjectGPS.ViewModel;
using System.Data.OleDb;

namespace ProjectGPS.Model
{


    public class SFReader : ISFReader
    {
        private ObservableCollection<Coordinates> _coordinates;
        private ObservableCollection<Place> _places;
        private ShapefileDataReader _sFDReader;
        public List<IFeature> features;
        private string _pathToFolder;
        private string _fileName;
        //  private ShapefileReader _reader;


        public SFReader()
        {
            Coordinates = new ObservableCollection<Coordinates>();
            Places = new ObservableCollection<Place>();
        }
        public void SaveShapeFile(ICollection<Coordinates> coordinates, string fileName)
        {
            var writer = new ShapefileDataWriter(fileName);
            writer.Header = new DbaseFileHeader();
            IList<IFeature> features = new List<IFeature>();
            foreach (var coordinate in coordinates)
            {
                features.Add(new Feature(new Point(coordinate.XValue, coordinate.YValue, 0.0), new AttributesTable()));
            }
            writer.Write(features);
        }
        private bool Contain(int id, Picture picture)
        {
            bool result = true;
            var toSearch = features.Where(x => Convert.ToInt32(x.Attributes["Id"]) == id).ToList();
            for (int i = 0; i < toSearch.Count; i++)
            {
                if (toSearch[i].Attributes["foto"].ToString() != picture.GetPicturePath)
                {
                    result = false;
                }
                else
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
        public void Save(ObservableCollection<Place> places)
        {
            foreach (var item in places)
            {
                foreach (var picture in item.Pictures)
                {
                    if (Contain(item.Id, picture) == false)
                    {
                        AttributesTable at = new AttributesTable();
                        at.AddAttribute("obwod", "");
                        at.AddAttribute("Ilosc_opra", "");
                        at.AddAttribute("foto2", "");
                        at.AddAttribute("Id", item.Id);
                        at.AddAttribute("foto", picture.GetPicturePath);
                        features.Add(new Feature(new Point(item.GetLongitudeValue, item.GetLatitudeValue), at));
                    }
                }
            }
            ShapefileDataWriter writer = new ShapefileDataWriter(_pathToFolder + FileName + ".shp", new GeometryFactory());
            DbaseFileHeader outDbaseHeader = ShapefileDataWriter.GetHeader((Feature)features[0], features.Count);
            writer.Header = outDbaseHeader;
            writer.Write(features);
        }
        public void ReadShapeFile()
        {
            features = new List<IFeature>();
            _sFDReader = new ShapefileDataReader(PathToFolder + FileName + ".shp", new GeometryFactory());
            DbaseFileHeader header = _sFDReader.DbaseHeader;
            while (_sFDReader.Read())
            {
                int id = -1;
                string pathToPicture = "";

                AttributesTable at = new AttributesTable();
                for (int j = 0; j < header.NumFields; j++)
                {
                    DbaseFieldDescriptor fldDescriptor = header.Fields[j];
                    at.AddAttribute(fldDescriptor.Name, _sFDReader.GetValue(j));
                    if (fldDescriptor.Name == "foto" && !string.IsNullOrEmpty(_sFDReader.GetValue(j).ToString()))
                    {
                        pathToPicture = _sFDReader.GetValue(j).ToString();
                    }
                    else if (fldDescriptor.Name == "Id")
                    {
                        id = Convert.ToInt32(_sFDReader.GetValue(j));
                    }
                }
                var geometry = (Geometry)_sFDReader.Geometry;
                features.Add(new Feature(geometry, at));

                if (Places.Any(x => x.Id == id))
                {
                    var result = Places.Where(x => x.Id == id).FirstOrDefault();
                    try
                    {
                        result.Pictures.Add(new Picture(pathToPicture));
                    }
                    catch (Exception) { }
                    Model.Coordinates coordinates = new Model.Coordinates(geometry.Coordinate.X, geometry.Coordinate.Y);
                }
                else
                {
                    var result = new Place();
                    result.SetCoordinates = new Model.Coordinates(geometry.Coordinate.X, geometry.Coordinate.Y);
                    result.Id = id;
                    try
                    {
                        result.Pictures.Add(new Picture(pathToPicture));
                    }
                    catch (Exception) { }
                    Places.Add(result);
                }
                //toAdd.SetCoordinates = new Model.Coordinates(geometry.Coordinate.X, geometry.Coordinate.Y);
                //Places.Add(toAdd);
            }
            CleanUp();
        }

        private void CleanUp()
        {
            try
            {
                _sFDReader.Close();
                _sFDReader.Dispose();
            }
            catch (Exception) { }
        }
        public ObservableCollection<Coordinates> Coordinates
        {
            get { return _coordinates; }
            set { _coordinates = value; }
        }
        public ObservableCollection<Place> Places
        {
            get { return _places; }
            set { _places = value; }
        }
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
        public string PathToFolder
        {
            get { return _pathToFolder; }
            set { _pathToFolder = value; }
        }
    }
}