﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Maps.MapControl.WPF;
using ProjectGPS.Model.Interfaces;

namespace ProjectGPS.Model
{
  
    public class PlaceInfo : IPlaceInfo
    {
        private Location _location;
        private ObservableCollection<Picture> _pictures;

        public Location Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public ObservableCollection<Picture> Pictures
        {
            get { return _pictures; }
            set { _pictures = value; }
        }
        public PlaceInfo()
        {
        }
    }

}
