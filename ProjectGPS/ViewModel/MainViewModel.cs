using MahApps.Metro.Controls;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.WindowsAPICodePack.Dialogs;
using ProjectGPS.Model;
using PropertyChanged;
using System;
using System.Collections.ObjectModel;
using System.IO;
using TweetSharp;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.Generic;
using NetTopologySuite.IO;
using NetTopologySuite.Geometries;
using Commander;
using ProjectGPS.Command;
using ProjectGPS.Converters;
using System.Windows;
using System.Windows.Interactivity;
using System.Diagnostics;
using ProjectGPS.View;
using ProjectGPS.Model.Interfaces;
using Microsoft.Win32;
using System.Windows.Media;
using System.Threading;

namespace ProjectGPS.ViewModel
{
    //TODO 
    //Unit testy
    // zapisywanie do db
    // odczywywanie z db
    // wczytywanie punktow z sf

    [ImplementPropertyChanged]
    public class MainViewModel : MetroWindow, IMainViewModel
    {
        private string _directory;
        private Location _center;
        private IPlace _selectedPlace;
        private double _distance;
        private bool _autoFind;
        private bool _isLoadingPlacePhotos;
        private bool _isLoadingShapeFile;
        private bool _isSelected;
        private bool _isflyOut;
        private bool _isLoadedShapeFile;



        private string _distanceInfo;
        private ObservableCollection<Place> _places;
        private ObservableCollection<Picture> _images;
        private ObservableCollection<Picture> _nearestImages;
        private readonly ISFReader _reader;
        private IPlaceInfo _placeInfo;
        private IShareInfo _shareInfo;

        public MainViewModel(ISFReader reader, IPlace selectedPlace, IPlaceInfo placeInfo, IShareInfo shareInfo)
        {
            IsLoadedShapeFile = false;
            IsflyOut = false;
            IsSelected = false;
            IsLoadingPlacePhotos = false;
            IsLoadingShapeFile = true;
            _reader = reader;
            _shareInfo = shareInfo;
            _placeInfo = placeInfo;
            _selectedPlace = selectedPlace;
            Center = new Location(50.3, 18.9);
            AutoFind = false;
            Distance = 10;
            DistanceInfo = "brak";
            Places = new ObservableCollection<Place>();
            NearestImages = new ObservableCollection<Picture>();
        }
        [OnCommand("SavePlaces")]
        private void savePlaces(object obj)
        {
            //todo save
            _reader.Save(Places);
        }
        [OnCommand("Share")]
        private void share(object obj)
        {
            ShareInfo.Location = new Location(SelectedPlace.GetLocation.Latitude, SelectedPlace.GetLocation.Longitude);
            ShareInfo.Pictures = NearestImages;
            ShareWindow shareWindow = new ShareWindow(ShareInfo);
            shareWindow.Show();
        }
        private async void LoadShapeFile()
        {
            await Task.Run(new Action(() =>
             {
                 OpenFileDialog openFileDialog = new OpenFileDialog();
                 openFileDialog.Filter = "Shape files (*.shp)|*.shp";
                 if (openFileDialog.ShowDialog() == true)
                 {
                     IsLoadingShapeFile = false;
                     var cuttedInfo = openFileDialog.FileName.Split(new string[] { ".shp" }, StringSplitOptions.None)[0];
                     int nrName = cuttedInfo.Split(new string[] { "\\" }, StringSplitOptions.None).Length - 1;
                     var fileName = cuttedInfo.Split(new string[] { "\\" }, StringSplitOptions.None)[nrName];
                     var pathToFolder = cuttedInfo.Split(new string[] { "\\" + fileName }, StringSplitOptions.None)[0] + "\\";
                     _reader.FileName = fileName;
                     _reader.PathToFolder = pathToFolder;
                     Directory = openFileDialog.FileName;
                     _reader.ReadShapeFile();
                     foreach (var item in _reader.Places.Take(10))
                     {
                         Application.Current.Dispatcher.Invoke(() =>
                         {
                             Places.Add(item);
                         });
                     }
                     CalclateCenter();
                 }
                 IsLoadingShapeFile = true;
             }));
        }

        [OnCommand("LoadPhotoFromFile")]
        private void loadPhotoFromFile(object obj)
        {
            if (AutoFind)
            {
                var openFileDialog = OpenFileDialog();
                if (openFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    Images = new ObservableCollection<Picture>();
                    var direcList = System.IO.Directory.GetFiles(openFileDialog.FileName, "*.jpg").ToList();
                    foreach (var item in direcList)
                    {
                        Images.Add(new Picture(item));
                    }
                }
            }
        }

        [OnCommand("OpenOptions")]
        private void openOptions(object obj)
        {
            IsflyOut = !IsflyOut;
        }
        [OnCommand("LoadShapeFile")]
        private void loadShapeFile(object obj)
        {
            LoadShapeFile();
            IsLoadedShapeFile = true;
        }
        [OnCommand("SelectPlace")]
        private async void selectPlace(Place obj)
        {

            IsSelected = true;
            IsLoadingPlacePhotos = true;
            var task = Task.Run(new Action(() =>
            {
                if (obj != null)
                {

                    var tmp = Places.Where(x => x.Color == Brushes.Violet).FirstOrDefault();
                    if (tmp != null)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {

                            Places.Remove(Places.Where(x => x.Color == Brushes.Violet).FirstOrDefault());
                            tmp.Color = Brushes.AliceBlue;
                            Places.Add(tmp);
                        });
                    }
                    if (AutoFind)
                    {

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            obj.Color = Brushes.Violet;
                            Places.Remove(obj);
                            Places.Add(obj);
                            FindNreaestPhotos(obj.GetLocation);
                        });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            obj.Color = Brushes.Violet;
                            Places.Remove(obj);
                            Places.Add(obj);
                            NearestImages = Places.Where(x => x == obj).FirstOrDefault().Pictures;
                        });
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {

                        SelectedPlace.Pictures = NearestImages;
                        SelectedPlace.SetCoordinates = new Coordinates(obj.GetLongitudeValue, obj.GetLatitudeValue);
                    });
                }
            }));
            await task;
            IsLoadingPlacePhotos = false;
        }
        [OnCommand("OpenEditWindow")]
        private void openEditWindow(object obj)
        {
            // PlaceInfo.Location = new Location(SelectedPlace.GetLocation.Latitude, SelectedPlace.GetLocation.Longitude);
            //  PlaceInfo.Pictures = SelectedPlace.Pictures;
            EditWindow editViewModel = new EditWindow();
            editViewModel.Show();
        }
        [OnCommand("OpenPircture")]
        private void openPicture(object obj)
        {
            if (obj is Picture && obj != null)
            {
                string arg = "\"C:\\Program Files (x86)\\Windows Photo Viewer\\PhotoViewer.dll\", ImageView_Fullscreen  " + (obj as Picture).GetPicturePath;
                System.Diagnostics.Process.Start("C:\\Windows\\System32\\rundll32.exe", arg);
            }
        }
        public async void CalclateCenter()
        {
            await Task.Run(new Action(() =>
            {
                double sumLat = Places.Select(x => x.GetLatitudeValue).Sum();
                double sumLng = Places.Select(x => x.GetLongitudeValue).Sum();
                Center = new Location(sumLat / (double)Places.Count, (double)sumLng / Places.Count);
            }));
        }
        private void FindNreaestPhotos(Location value)
        {
            NearestImages = new ObservableCollection<Picture>();
            Images.Select(x => { x.Distance = new Distance(x.GetLocation, value).Value; return x; }).ToList();
            NearestImages = ObservableCollectionConverter.ToObservableCollection(Images.OrderByDescending(x => x.Distance).Take(10));
        }
        public ObservableCollection<Place> Places
        {
            get { return _places; }
            set { _places = value; }
        }
        public string Directory
        {
            get { return _directory; }
            set { _directory = value; }
        }
        public IPlace SelectedPlace
        {
            get { return _selectedPlace; }
            set { _selectedPlace = value; }
        }
        public ObservableCollection<Picture> Images
        {
            get { return _images; }
            set { _images = value; }
        }
        public ObservableCollection<Picture> NearestImages
        {
            get { return _nearestImages; }
            set { _nearestImages = value; }
        }
        public Location Center
        {
            get { return _center; }
            set { _center = value; }
        }
        public string DistanceInfo
        {
            get { return "Odleglosc: " + Convert.ToInt32(Distance).ToString() + "m"; }
            set { _distanceInfo = value; }
        }
        public bool AutoFind
        {
            get { return _autoFind; }
            set { _autoFind = value; }
        }
        public double Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }
        public IPlaceInfo PlaceInfo
        {
            get { return _placeInfo; }
            set { _placeInfo = value; }
        }
        public IShareInfo ShareInfo
        {
            get { return _shareInfo; }
            set { _shareInfo = value; }
        }
        public bool IsLoadingShapeFile
        {
            get { return _isLoadingShapeFile; }
            set { _isLoadingShapeFile = value; }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        public bool IsLoadingPlacePhotos
        {
            get { return _isLoadingPlacePhotos; }
            set { _isLoadingPlacePhotos = value; }
        }

        public bool IsflyOut
        {
            get { return _isflyOut; }
            set { _isflyOut = value; }
        }

        public bool IsLoadedShapeFile
        {
            get
            {
                return _isLoadedShapeFile;
            }

            set
            {
                _isLoadedShapeFile = value;
            }
        }

        private CommonOpenFileDialog OpenFileDialog()
        {
            var result = new CommonOpenFileDialog();
            result.Title = "Otw�rz";
            result.Multiselect = false;
            result.IsFolderPicker = true;
            result.ShowPlacesList = true;
            result.EnsureReadOnly = false;
            result.EnsureFileExists = true;
            result.EnsurePathExists = true;
            result.EnsureValidNames = true;
            result.DefaultDirectory = @"C:\";
            result.AllowNonFileSystemItems = false;
            result.AddToMostRecentlyUsedList = false;
            return result;
        }
    }
}
