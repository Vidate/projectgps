﻿using Commander;
using MahApps.Metro.Controls;
using ProjectGPS.Command;
using ProjectGPS.Model;
using ProjectGPS.View.Windows;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TweetSharp;

namespace ProjectGPS.ViewModel
{
    [ImplementPropertyChanged]
    class ShareViewModel : MetroWindow
    {
        private readonly TwitterService _service;
        private OAuthRequestToken _requestToken;
        private bool _isSending;

        public bool IsSending
        {
            get { return _isSending; }
            set { _isSending = value; }
        }

        private bool _authorization;
        private ObservableCollection<Picture> _pictures;
        public ShareViewModel(IShareInfo obj)
        {
            _pictures = obj.Pictures;
            string consumerKey = System.Configuration.ConfigurationManager.AppSettings["ConsumerKey"];
            string consumerSecret = System.Configuration.ConfigurationManager.AppSettings["ConsumerSecret"];
            _service = new TwitterService(consumerKey, consumerSecret);
            CheckActivation();
        }
        private void CheckActivation()
        {

            //string result = System.Configuration.ConfigurationManager.AppSettings["ActivationToken"];
            //if (result == "-1")
            //{
            Authorization = true;
            _requestToken = _service.GetRequestToken();
            Uri uri = _service.GetAuthorizationUri(_requestToken);
            Process.Start(uri.ToString());
            //}
            //else
            //{
            //    Authorization = false;
            //    _requestToken = _service.GetRequestToken();
            //    OAuthAccessToken access = _service.GetAccessToken(_requestToken, result);
            //    _service.AuthenticateWith(access.Token, access.TokenSecret);
            //}
        }

        [OnCommand("SendTweet")]
        private void sendTweet(object description)
        {
            SendTweetAsync(description, Pictures);
        }

        private async void SendTweetAsync(object description, ObservableCollection<Picture> toSend)
        {
            var task = Task.Run(() =>
            {
                IsSending = true;
                if (toSend.Count != 0)
                {
                    foreach (var item in toSend)
                    {
                        var tweetWithMedia = new SendTweetWithMediaOptions() { Status = description as string };
                        tweetWithMedia.Images = new Dictionary<string, Stream>();
                        tweetWithMedia.Images.Add("Photo" + item.GetPicturePath, new FileStream(item.GetPicturePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                        IAsyncResult result = _service.BeginSendTweetWithMedia(tweetWithMedia);
                        var tweets = _service.EndSendTweetWithMedia(result);
                    }
                }
                else
                {
                    var tweetWithMedia = new SendTweetWithMediaOptions() { Status = description as string };
                    tweetWithMedia.Images = new Dictionary<string, Stream>();
                    tweetWithMedia.Images.Add("Photo" + @"C:\Users\Piotr\Desktop\images.png", new FileStream(@"C:\Users\Piotr\Desktop\images.png", FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                    IAsyncResult result = _service.BeginSendTweetWithMedia(tweetWithMedia);
                    var tweets = _service.EndSendTweetWithMedia(result);
                }
                Application.Current.Dispatcher.Invoke(() =>
                {
                    MyDialog d = new MyDialog();
                    d.Show();
                });
            });
            await task;
            IsSending = false;
        }

        [OnCommand("ReadToken")]
        private void readToken(object obj)
        {
            var config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["ActivationToken"].Value = obj as string;
            config.Save(ConfigurationSaveMode.Modified);
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");

            string verifier = obj.ToString();
            OAuthAccessToken access = _service.GetAccessToken(_requestToken, verifier);
            _service.AuthenticateWith(access.Token, access.TokenSecret);
            Authorization = false;
        }

        public bool Authorization
        {
            get { return _authorization; }
            set { _authorization = value; }
        }

        public ObservableCollection<Picture> Pictures
        {
            get { return _pictures; }
            set { _pictures = value; }
        }

    }
}
