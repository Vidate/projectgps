﻿using Aurigma.GraphicsMill.Codecs;
using Commander;
using Microsoft.WindowsAPICodePack.Dialogs;
using ProjectGPS.Command;
using ProjectGPS.Converters;
using ProjectGPS.Model;
using ProjectGPS.Model.Interfaces;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;


namespace ProjectGPS.ViewModel
{
    [ImplementPropertyChanged]
    public class EditViewModel
    {
        private ObservableCollection<Picture> _editedPictures;
        private ObservableCollection<Picture> _originalPictures;
        private ObservableCollection<Picture> _picturesFromFolder;
        private string _directory;
        private IPlace _place;

        public ObservableCollection<Picture> EditedPictures
        {
            get { return _editedPictures; }
            set { _editedPictures = value; }
        }
        public ObservableCollection<Picture> OriginalPictures
        {
            get { return _originalPictures; }
            set { _originalPictures = value; }
        }
        public ObservableCollection<Picture> PicturesFromFolder
        {
            get { return _picturesFromFolder; }
            set { _picturesFromFolder = value; }
        }
        public string Directory
        {
            get { return _directory; }
            set { _directory = value; }
        }

        public IPlace Place
        {
            get { return _place; }
            set { _place = value; }
        }
        public EditViewModel(IPlace place)
        {
            _place = place;
            OriginalPictures = new ObservableCollection<Picture>();
            EditedPictures = new ObservableCollection<Picture>();
            PicturesFromFolder = new ObservableCollection<Picture>();
            OriginalPictures = _place.Pictures;
            foreach (var item in _place.Pictures)
            {
                EditedPictures.Add(new Picture(item.GetPicturePath));
            }
        }
        //public ICommand RotateLeft
        //{
        //    //Image i = Image.FromFile("file.jpg")
        //    //img.RotateFlip(RotateFlipType.Rotate90FlipNone)
        //    //img.Save("file.jpg");
        //    get { return new RelayCommand(rotateLeft, x => true); }
        //}
        //private async void rotateLeft(object obj)
        //{
        //    var task = Task.Run(() =>
        //    {
        //        //TODO!!!
        //        Application.Current.Dispatcher.Invoke(() =>
        //        {
        //            throw new ArgumentException("Co gdzie jak za ile?");
        //            //var tmp = obj as Picture;
        //            //var path = tmp.GetPicturePath;
        //            //var pathWithoutjpg = path.Split(new string[] { ".jpg" }, StringSplitOptions.None)[0];

        //            //using (Stream s = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        //            //{
        //            //    using (var losslessJpeg = new LosslessJpeg(s))
        //            //    {
        //            //        losslessJpeg.WriteRotated(pathWithoutjpg + "Rotate90.jpg", System.Drawing.RotateFlipType.Rotate90FlipNone);
        //            //    }
        //            //}
        //            //EditedPictures.Remove(EditedPictures.Where(x => x.GetPicturePath == path).FirstOrDefault());
        //            //_place.Pictures.Clear();
        //            //deleteFile(path);
        //            //chalangeName(pathWithoutjpg + "Rotate90.jpg", path, pathWithoutjpg + "Rotate90old.jpg");
        //            //EditedPictures.Add(new Picture(path));


        //            //foreach (var item in EditedPictures)
        //            //{
        //            //    _place.Pictures.Add(item);
        //            //}
        //        });
        //    });
        //    await task;

        //}
        private void deleteFile(string path)
        {
            PowerShell.Create().AddCommand("Remove-Item")
                     .AddParameter("Path", path)
                     .Invoke();
        }
        private void chalangeName(string oldName, string newName, string newName2)
        {
            PowerShell.Create().AddCommand("Copy-Item")
                         .AddParameter("Path", newName)
                         .AddParameter("Destination", newName2)
                         .Invoke();
            PowerShell.Create().AddCommand("Move-Item")
                        .AddParameter("Path", oldName)
                        .AddParameter("Destination", newName)
                        .Invoke();
        }
        //[OnCommand("Save")]
        public ICommand Save
        {
            get { return new RelayCommand(save, x => true); }
        }
        private void save(object obj)
        {
            _place.Pictures.Clear();
            foreach (var item in EditedPictures)
            {
                _place.Pictures.Add(item);
            }
        }
        //[OnCommand("AddPicture")]
        public ICommand AddPicture
        {
            get { return new RelayCommand(addPicture, x => true); }
        }
        private void addPicture(object obj)
        {
            EditedPictures.Add(obj as Picture);
            PicturesFromFolder.Remove(obj as Picture);
        }
        //[OnCommand("RemovePicture")]
        public ICommand RemovePicture
        {
            get { return new RelayCommand(removePicture, x => true); }
        }
        private void removePicture(object obj)
        {
            EditedPictures.Remove(obj as Picture);
            PicturesFromFolder.Add(obj as Picture);
        }
        //[OnCommand("LoadImage")]
        public ICommand LoadImage
        {
            get { return new RelayCommand(loadImage, x => true); }
        }
        private void loadImage(object obj)
        {
            var openFileDialog = OpenFileDialog();
            if (openFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                PicturesFromFolder = new ObservableCollection<Picture>();
                Directory = openFileDialog.FileName;
            }
            var direcList = System.IO.Directory.GetFiles(Directory, "*.jpg").ToList();
            FillPictuersFromFolder(direcList);
        }
        private async void FillPictuersFromFolder(List<string> direcList)
        {
            await Task.Run(new Action(() =>
            {
                foreach (var item in direcList)
                {
                    if (!(EditedPictures.Any(x => x.GetPicturePath.ToLower() == item.ToLower())))
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            PicturesFromFolder.Add(new Picture(item));
                        });
                    }
                }
            }));
        }
        private CommonOpenFileDialog OpenFileDialog()
        {
            var result = new CommonOpenFileDialog();
            result.Title = "Otwórz";
            result.Multiselect = false;
            result.IsFolderPicker = true;
            result.ShowPlacesList = true;
            result.EnsureReadOnly = false;
            result.EnsureFileExists = true;
            result.EnsurePathExists = true;
            result.EnsureValidNames = true;
            result.DefaultDirectory = @"C:\";
            result.AllowNonFileSystemItems = false;
            result.AddToMostRecentlyUsedList = false;
            return result;
        }
    }
}

