﻿using System.Collections.ObjectModel;
using Microsoft.Maps.MapControl.WPF;
using ProjectGPS.Model;
using System;
using ProjectGPS.Model.Interfaces;

namespace ProjectGPS.ViewModel
{
    public interface IMainViewModel
    {
        bool AutoFind { get; set; }
        Location Center { get; set; }
        string Directory { get; set; }
        double Distance { get; set; }
        string DistanceInfo { get; set; }
        ObservableCollection<Picture> Images { get; set; }
        ObservableCollection<Picture> NearestImages { get; set; }
        ObservableCollection<Place> Places { get; set; }
        IPlace SelectedPlace { get; set; }
    }
}