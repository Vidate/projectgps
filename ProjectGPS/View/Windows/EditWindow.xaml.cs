﻿using MahApps.Metro.Controls;
using ProjectGPS.Model;
using ProjectGPS.Model.Interfaces;
using ProjectGPS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectGPS.View
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : MetroWindow
    {
        public EditWindow()
        {
            var dataContext = (MainViewModel)Application.Current.MainWindow.DataContext;
            this.DataContext = new EditViewModel(dataContext.SelectedPlace);
            InitializeComponent();
        }
    }
}
