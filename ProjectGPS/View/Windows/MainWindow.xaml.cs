﻿using Autofac;
using Autofac.Core;
using MahApps.Metro.Controls;
using Microsoft.Maps.MapControl.WPF;
using Microsoft.WindowsAPICodePack.Dialogs;
using ProjectGPS.Model;
using ProjectGPS.Model.Interfaces;
using ProjectGPS.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ProjectGPS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        public MainWindow()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<SFReader>().As<ISFReader>();
            builder.RegisterType<Place>().As<IPlace>();
            builder.RegisterType<PlaceInfo>().As<IPlaceInfo>();
            builder.RegisterType<ShareInfo>().As<IShareInfo>();
            builder.RegisterType<MainViewModel>().As<IMainViewModel>();

            var container = builder.Build();
            var viewModel = container.Resolve<IMainViewModel>();
            this.DataContext = viewModel;
            InitializeComponent();
        }

        private void Pushpin_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            (sender as Pushpin).Background = Brushes.Blue;
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
            Close();
            Process.GetCurrentProcess().Kill();
        }
    }


}

