﻿using MahApps.Metro.Controls;
using ProjectGPS.Model;
using ProjectGPS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectGPS
{
    /// <summary>
    /// Interaction logic for -+.xaml
    /// </summary>
    public partial class ShareWindow : MetroWindow
    {
        public ShareWindow(IShareInfo info)
        {

            this.DataContext = new ShareViewModel(info);
            InitializeComponent();
        }
    }
}
